import shutil
import os
import sys
from cx_Freeze import setup, Executable

excludes = ["Tkconstants", 'Tkinter', 'tcl']
includes = ["atexit", "PySide.QtNetwork"]

exe = Executable(
    script="etalon.pyw",
    base="Win32GUI",
    icon="res/icons/book.ico"
    )
 
setup(
    name = "Etalon",
    version = "0.2",
    description = "Account book for Etalon building",
    options = {"build_exe": {"includes":includes, "excludes": excludes}},
    executables = [exe]
    )

#shutil.copy(os.getcwd() + '/fix/QtCore4.dll', os.getcwd() + '/build/exe.win32-2.7/')
#shutil.copy(os.getcwd() + '/fix/QtGui4.dll', os.getcwd() + '/build/exe.win32-2.7/')
