#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  classes.py
#  
#  Copyright 2012 Said B <phoenix49@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import copy
import urllib2
import subprocess
from PySide.QtGui import *
from PySide.QtCore import *
import ui_dialog
import ui_resident
import ui_about

class Block(object):
    def __init__(self, label, flats, floors, parent=None):
        self.flats = {} #flatnumber:flat
        self.floors = floors
        self.label = label
        self.blockid = 0 #unique id for block
        for flatno in range(int(flats)):
            self.addFlat(flatno + 1) #To fix flat number 0 issue
    
    def addFlat(self, number):
        self.flats[number] = Flat(number)
        
    def removeFlat(self, flatnumber):
        #do not use this
        self.flats.pop(flatnumber)

class Flat(object):
    def __init__(self, number, parent=None):
        self.number = number
        self.owner = 0 #humanid
        self.residents = 1 #Number of people
        self.area = 0
        self.rooms = 0
        self.block = 0 #blockid
        self.counters = {} #year:months:resources - Counters for resources
    
    def updateCounter(self, year, month, resname, value):
        #Resname is resourcename, or payed
        if not year in self.counters:
            self.counters[year] = {}
        if not month in self.counters[year]:
            self.counters[year][month] = {}
        self.counters[year][month][resname] = value
    
    def getCounter(self, year, month, resname):
        try:
            return self.counters[year][month][resname]
        except:
            return 0
    
    def addResident(self, id, human):
        self.residents[id] = human
    
    def removeResident(self, humanid):
        del self.residents[humanid]

class People(object):
    def __init__(self, parent=None):
        self.pack = {}
        self.uniqueid = 1
    
    def addHuman(self, name, surname, phone, altphone, flat, block):
        self.pack[self.uniqueid] = Human(self.uniqueid, name, surname, phone, altphone, flat, block)
        self.uniqueid += 1
    
    def editHuman(self, name, surname, phone, altphone, flat, block, id):
        self.pack[id] = Human(id, name, surname, phone, altphone, flat, block)
    
    def removeHuman(self, id):
        self.pack.pop(id)
    
    def getName(self, id):
        info = self.pack[id].name + ' ' + self.pack[id].surname
        return info
    
    def clearAll(self):
        self.pack = {}

class Human(object):
    def __init__(self, humanid, name, surname, phone, altphone, flat, block, parent=None):
        self.id = humanid
        self.name = name
        self.surname = surname
        self.phone = phone
        self.altphone = altphone
        self.livesinflat = flat #flatid , not a flat number
        self.livesinblock = block
        self.sex = 0 #0 - unknown, 1 - male, 2 - female

class Resource(object):
    def __init__(self, name, price, parent=None):
        self.name = name
        self.price = price

class Dialog(QDialog, ui_dialog.Ui_Dialog):
    def __init__(self, parent=None):
        super(Dialog, self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.blocks = 0
    
    def prepareColumns(self, headerlist):
        for i in range(len(headerlist)):
            self.tableWidget.insertColumn(i)
        self.tableWidget.setHorizontalHeaderLabels(headerlist)
    
    def updateView(self, items):
        #items should be a list of rows containing list of columns
        for row in range(len(items)):
            if row >= self.tableWidget.rowCount():
                self.tableWidget.insertRow(row)
            for column in range(len(items[row])):
                self.tableWidget.setItem(row, column, QTableWidgetItem(items[row][column]))

class ResDialog(QDialog):
    def __init__(self, title, parent=None):
        super(ResDialog, self).__init__(parent)
        self.setModal(True)
        self.setWindowTitle(title)
        self.label = QLabel(self.tr('Name: '))
        self.label2 = QLabel(self.tr('Price: '))
        self.name = QLineEdit()
        self.price = QDoubleSpinBox()
        self.price.setRange(0, 99)
        self.price.setSingleStep(0.01)
        self.okButton = QPushButton('OK')
        self.cancelButton = QPushButton(self.tr('Cancel'))
        
        #Layouts
        hbox = QHBoxLayout()
        hbox.addWidget(self.okButton)
        hbox.addWidget(self.cancelButton)
        formlayout = QFormLayout()
        formlayout.addRow(self.label, self.name)
        formlayout.addRow(self.label2, self.price)
        formlayout.addRow(hbox)
        self.setLayout(formlayout)

class Resources(Dialog):
    def __init__(self, resources, parent=None):
        super(Resources, self).__init__(parent)
        self.origres = resources
        self.resources = copy.deepcopy(resources)
        self.setWindowTitle(self.tr('Resources'))
        headers = [self.tr('Name'), self.tr('Price')]
        self.prepareColumns(headers)
        self.populateList()
        #Signals
        self.addButton.clicked.connect(self.newResource)
        self.editButton.clicked.connect(self.editResource)
        self.removeButton.clicked.connect(self.remove)
        self.removeallButton.clicked.connect(self.removeAll)
    
    def newResource(self):
        self.win = ResDialog(self.tr('New resource'), self)
        self.win.okButton.clicked.connect(self.addRes)
        self.win.cancelButton.clicked.connect(self.win.close)
        self.win.show()
    
    def addRes(self):
        if not self.win.name.text() == '':
            a = Resource(self.win.name.text(), self.win.price.value())
        self.resources.append(a)
        self.populateList()
        self.win.close()
    
    def editResource(self):
        row = self.tableWidget.currentRow()
        self.win = ResDialog(self.tr('Edit resource'), self)
        self.win.name.setText(self.resources[row].name)
        self.win.price.setValue(float(self.resources[row].price))
        self.win.okButton.clicked.connect(self.changeRes)
        self.win.cancelButton.clicked.connect(self.win.close)
        self.win.show()
    
    def changeRes(self):
        row = self.tableWidget.currentRow()
        self.resources[row] = Resource(self.win.name.text(), self.win.price.value())
        self.populateList()
        self.win.close()
    
    def remove(self):
        row = self.tableWidget.currentRow()
        name = self.tableWidget.item(row, 0).text()
        q = QMessageBox.question(self, self.tr('Remove'), \
                self.tr('Are you sure you wish to delete {0}?'.format(name)), QMessageBox.Ok | QMessageBox.Cancel)
        if q == QMessageBox.Ok:
            del self.resources[row]
            self.tableWidget.removeRow(row)
    
    def removeAll(self):
        q = QMessageBox.question(self, self.tr('Remove'), \
                self.tr('Are you sure you wish to delete all resources?'), QMessageBox.Ok | QMessageBox.Cancel)
        if q == QMessageBox.Ok:
            self.resources = []
            for row in sorted(range(self.tableWidget.rowCount()), None, None, True):
                self.tableWidget.removeRow(row)
    
    def populateList(self):
        rows = []
        for res in self.resources:
            columns = []
            columns.append(res.name)
            columns.append(str(res.price))
            rows.append(columns)
        self.updateView(rows)
        
class Months(Dialog):
    def __init__(self, account, parent=None):
        super(Months, self).__init__(parent)
        self.origacc = account
        self.account = copy.deepcopy(account)
        self.setWindowTitle(self.tr('Months'))
        headers = [self.tr('Year'), self.tr('Month')]
        self.prepareColumns(headers)
        self.populateList()
        #Signals
        self.addButton.clicked.connect(self.newMonth)
        self.editButton.clicked.connect(self.editMonth)
        self.removeButton.clicked.connect(self.remove)
        self.removeallButton.clicked.connect(self.removeAll)

    def remove(self):
        row = self.tableWidget.currentRow()
        year = int(self.tableWidget.item(row, 0).text())
        month = int(self.tableWidget.item(row, 1).text())
        q = QMessageBox.question(self, self.tr('Remove'), \
                self.tr('Are you sure you wish to delete {0}?'.format('\n'+str(year)+' '+str(month))), QMessageBox.Ok | QMessageBox.Cancel)
        if q == QMessageBox.Ok:
            self.account.removeMonth(year, month)
            self.tableWidget.removeRow(row)
    
    def removeAll(self):
        q = QMessageBox.question(self, self.tr('Remove'), \
                self.tr('Are you sure you wish to delete all months?'), QMessageBox.Ok | QMessageBox.Cancel)
        if q == QMessageBox.Ok:
            self.account.data = {}
            for row in sorted(range(self.tableWidget.rowCount()), None, None, True):
                self.tableWidget.removeRow(row)
    
    def editMonth(self):
        row = self.tableWidget.currentRow()
        year = self.tableWidget.item(row, 0).text()
        month = self.tableWidget.item(row, 1).text()
        self.win = MonthDialog(self.tr('Edit month'), self)
        self.win.year.setValue(int(year))
        self.win.month.setValue(int(month))
        self.win.okButton.clicked.connect(self.changeMonth)
        self.win.cancelButton.clicked.connect(self.win.close)
        self.win.show()
    
    def changeMonth(self):
        row = self.tableWidget.currentRow()
        oldyear = int(self.tableWidget.item(row, 0).text())
        oldmonth = int(self.tableWidget.item(row, 1).text())
        newyear = int(self.win.year.value())
        newmonth = int(self.win.month.value())
        self.account.editMonth(oldyear, oldmonth, newyear, newmonth)
        self.populateList()
        self.win.close()
    
    def newMonth(self):
        self.win = MonthDialog(self.tr('New month'), self)
        self.win.okButton.clicked.connect(self.addMonth)
        self.win.cancelButton.clicked.connect(self.win.close)
        self.win.show()
    
    def addMonth(self):
        year = self.win.year.value()
        month = self.win.month.value()
        self.account.newMonth(year, month)
        self.win.close()
        self.populateList()
        
    def populateList(self):
        rows = []
        for year in sorted(self.account.data.iterkeys()):
            for month in self.account.data[year]:
                columns = []
                columns.append(str(year))
                columns.append(str(month))
                rows.append(columns)
        self.updateView(rows)

class MonthDialog(QDialog):
    def __init__(self, title, parent=None):
        super(MonthDialog, self).__init__(parent)
        self.setModal(True)
        self.setWindowTitle(title)
        self.label = QLabel(self.tr('Year: '))
        self.label2 = QLabel(self.tr('Month: '))
        self.year = QSpinBox()
        self.year.setRange(2012, 2020)
        self.month = QSpinBox()
        self.month.setRange(1, 12)
        self.okButton = QPushButton('OK')
        self.cancelButton = QPushButton(self.tr('Cancel'))
        
        #Layouts
        hbox = QHBoxLayout()
        hbox.addWidget(self.okButton)
        hbox.addWidget(self.cancelButton)
        formlayout = QFormLayout()
        formlayout.addRow(self.label, self.year)
        formlayout.addRow(self.label2, self.month)
        formlayout.addRow(hbox)
        self.setLayout(formlayout)
    
class FlatDialog(QDialog):
    #future
    def __init__(self, title, parent=None):
        super(BlockDialog, self).__init__(parent)
        self.setModal(True)
        self.setWindowTitle(title)
        self.label = QLabel(self.tr('Flat number: '))
        self.number = QLineEdit()
        self.number.setInputMask('D00')
        self.label2 = QLabel(self.tr('Flat owner: '))
        self.owner = QComboBox()
        self.label3 = QLabel(self.tr('Number of residents: '))
        self.residents = QLineEdit()
        self.residents.setInputMask('D')
        self.label4 = QLabel(self.tr('Area: '))
        self.area = QLineEdit()
        self.area.setInputMask('DD0')
        self.label5 = QLabel(self.tr('Rooms: '))
        self.rooms = QLineEdit()
        self.rooms.setInputMask('D')
        self.okButton = QPushButton('OK')
        self.cancelButton = QPushButton(self.tr('Cancel'))
        
        #Layouts
        hbox = QHBoxLayout()
        hbox.addWidget(self.okButton)
        hbox.addWidget(self.cancelButton)
        formlayout = QFormLayout()
        formlayout.addRow(self.label, self.number)
        formlayout.addRow(self.label2, self.owner)
        formlayout.addRow(self.label3, self.residents)
        formlayout.addRow(self.label4, self.area)
        formlayout.addRow(self.label5, self.rooms)
        formlayout.addRow(hbox)
        self.setLayout(formlayout)


class Flats(Dialog):
    #future
    def __init__(self, blocks, people, parent=None):
        super(Flats, self).__init__(parent)
        self.blocks = blocks
        self.people = people
        self.setWindowTitle(self.tr('Flats'))
        headers = [self.tr('Block'), self.tr('Flat'), self.tr('Owner')]
        self.prepareColumns(headers)
        self.tableWidget.setColumnWidth(0, 40)
        self.tableWidget.setColumnWidth(1, 45)
        self.tableWidget.setColumnWidth(2, 300)
        self.setMinimumWidth(560)
        self.populateList()
    
    def populateList(self):
        rows = []
        for block in sorted(self.blocks.iterkeys()):
            for flatno in self.blocks[block].flats.iterkeys():
                columns = []
                columns.append(block) #Block name
                columns.append(str(flatno))
                ownerid = self.blocks[block].flats[flatno].owner
                if ownerid > 0:
                    columns.append(self.people.getName(ownerid))
                else:
                    columns.append('')
                rows.append(columns)
        self.updateView(rows)

class Resident(QDialog, ui_resident.Ui_Dialog):
    def __init__(self, blocks, flat=None, block=None, parent=None):
        super(Resident, self).__init__(parent)
        self.setupUi(self)
        self.blocks = blocks
        self.updateBlockCombo()
        self.updateFlats()
        self.block.currentIndexChanged.connect(self.updateFlats)
        if block is not None:
            self.block.setCurrentIndex(self.block.findText(block))
        if flat is not None:
            if flat == '':
                flat = 0
            self.flat.setCurrentIndex(int(flat)-1)
        
    def updateBlockCombo(self):
        #Populate blocks and sort them
        for block in sorted(self.blocks.iterkeys()):
            self.block.addItem(self.blocks[block].label)
    
    def updateFlats(self, *args):
        self.flat.clear()
        cb = self.block.currentText() #Selected block
        try:
            for flat in self.blocks[cb].flats.iterkeys():
                self.flat.addItem(str(flat))
        except:
            pass

class BlockDialog(QDialog):
    def __init__(self, title, parent=None):
        super(BlockDialog, self).__init__(parent)
        self.setModal(True)
        self.setWindowTitle(title)
        self.label = QLabel(self.tr('Block name: '))
        self.name = QLineEdit()
        self.label2 = QLabel(self.tr('Number of flats: '))
        self.flats = QLineEdit()
        self.flats.setInputMask('D00')
        self.label3 = QLabel(self.tr('Number of floors: '))
        self.floors = QLineEdit()
        self.floors.setInputMask('D0')
        self.okButton = QPushButton('OK')
        self.cancelButton = QPushButton(self.tr('Cancel'))
        
        #Layouts
        hbox = QHBoxLayout()
        hbox.addWidget(self.okButton)
        hbox.addWidget(self.cancelButton)
        formlayout = QFormLayout()
        formlayout.addRow(self.label, self.name)
        formlayout.addRow(self.label3, self.floors)
        formlayout.addRow(self.label2, self.flats)
        formlayout.addRow(hbox)
        self.setLayout(formlayout)
        
class Blocks(Dialog):
    def __init__(self, blocks, parent=None):
        super(Blocks, self).__init__(parent)
        self.origdata = blocks
        self.data = copy.deepcopy(blocks)
        self.setWindowTitle(self.tr('Blocks'))
        headers = [self.tr('Name'), self.tr('Number of flats'), self.tr('Number of floors')]
        self.prepareColumns(headers)
        self.tableWidget.setColumnWidth(0, 50)
        self.tableWidget.setColumnWidth(1, 150)
        self.tableWidget.setColumnWidth(2, 150)
        self.setMinimumWidth(500)
        self.populateList()
        #Signals
        self.addButton.clicked.connect(self.newBlock)
        self.editButton.clicked.connect(self.editBlock)
        self.removeButton.clicked.connect(self.remove)
        self.removeallButton.clicked.connect(self.removeAll)
    
    def remove(self):
        row = self.tableWidget.currentRow()
        bl = self.tableWidget.item(row, 0).text()
        q = QMessageBox.question(self, self.tr('Remove'), \
                self.tr('Are you sure you wish to delete {0}?'.format(str(bl))), QMessageBox.Ok | QMessageBox.Cancel)
        if q == QMessageBox.Ok:
            self.data.pop(bl)
            self.tableWidget.removeRow(row)
    
    def removeAll(self):
        q = QMessageBox.question(self, self.tr('Remove'), \
                self.tr('Are you sure you wish to delete all blocks?'), QMessageBox.Ok | QMessageBox.Cancel)
        if q == QMessageBox.Ok:
            self.data = {}
            for row in sorted(range(self.tableWidget.rowCount()), None, None, True):
                self.tableWidget.removeRow(row)
    
    def editBlock(self):
        row = self.tableWidget.currentRow()
        bl = self.tableWidget.item(row, 0).text()
        self.win = BlockDialog(self.tr('Edit Block'), self)
        self.win.name.setText(self.data[bl].label)
        self.win.flats.setText(str(len(self.data[bl].flats)))
        self.win.floors.setText(str(self.data[bl].floors))
        self.win.okButton.clicked.connect(self.createBlock)
        self.win.cancelButton.clicked.connect(self.win.close)
        self.win.show()
    
    def newBlock(self):
        self.win = BlockDialog(self.tr('New Block'), self)
        self.win.okButton.clicked.connect(self.createBlock)
        self.win.cancelButton.clicked.connect(self.win.close)
        self.win.show()
    
    def createBlock(self):
        if not self.win.name.text() == '':
            self.data[str(self.win.name.text())] = Block(self.win.name.text(), self.win.flats.text(), self.win.floors.text())
        self.win.close()
        self.populateList()
        
    def populateList(self):
        rows = []
        for item in sorted(self.data.iterkeys()):
            columns = []
            columns.append(self.data[item].label)
            columns.append(str(len(self.data[item].flats)))
            columns.append(str(self.data[item].floors))
            rows.append(columns)
        self.updateView(rows)

class Residents(Dialog):
    def __init__(self, people, blocks, parent=None):
        super(Residents, self).__init__(parent)
        self.origdata = people
        self.data = copy.deepcopy(people) #Create copy of people and work on it
        self.blocks = blocks
        self.setWindowTitle(self.tr('Residents'))
        self.tableWidget.insertColumn(0) #Flat
        self.tableWidget.insertColumn(1) #Block
        self.tableWidget.insertColumn(2) #Info
        self.tableWidget.insertColumn(3) #Hidden column for humanid
        self.tableWidget.hideColumn(3)
        headers = [self.tr('Flat'), self.tr('Block'), self.tr('Info'), 'Humanid']
        self.tableWidget.setHorizontalHeaderLabels(headers)
        self.tableWidget.setColumnWidth(0, 40)
        self.tableWidget.setColumnWidth(1, 45)
        self.tableWidget.setColumnWidth(2, 400)
        self.setMinimumWidth(640)
        self.populateList()
        #Signals
        self.addButton.clicked.connect(self.newResident)
        self.editButton.clicked.connect(self.editResident)
        self.removeButton.clicked.connect(self.remove)
        self.removeallButton.clicked.connect(self.removeAll)
    
    def remove(self):
        row = self.tableWidget.currentRow()
        humanid = int(self.tableWidget.item(row, 3).text())
        q = QMessageBox.question(self, self.tr('Remove'), \
                self.tr('Are you sure you wish to delete {0}?'.format(self.data.getName(humanid))), QMessageBox.Ok | QMessageBox.Cancel)
        if q == QMessageBox.Ok:
            self.data.removeHuman(humanid)
            self.tableWidget.removeRow(row)
    
    def removeAll(self):
        q = QMessageBox.question(self, self.tr('Remove'), \
                self.tr('Are you sure you wish to delete all people?'), QMessageBox.Ok | QMessageBox.Cancel)
        if q == QMessageBox.Ok:
            self.data.clearAll()
            for row in sorted(range(self.tableWidget.rowCount()), None, None, True):
                self.tableWidget.removeRow(row)
    
    def populateList(self):
        rows = []
        for item in self.data.pack.itervalues():
            columns = []
            columns.append(item.livesinflat)
            columns.append(item.livesinblock)
            if len(item.phone) > 0:
                phstr = ', {0}'.format(item.phone)
            else:
                phstr = ''
            columns.append(item.name + ' ' + item.surname + phstr)
            columns.append(str(item.id))
            rows.append(columns)
        self.updateView(rows)
        self.tableWidget.sortItems(0)
    
    def editResident(self):
        row = self.tableWidget.currentRow()
        #Find out humanid of selected row
        humanid = int(self.tableWidget.item(row, 3).text())
        self.win = Resident(self.blocks, self.data.pack[humanid].livesinflat, \
                    self.data.pack[humanid].livesinblock, self)
        self.win.name.setText(self.data.pack[humanid].name)
        self.win.surname.setText(self.data.pack[humanid].surname)
        self.win.phone.setText(self.data.pack[humanid].phone)
        self.win.altphone.setText(self.data.pack[humanid].altphone)
        self.win.accepted.connect(self.updateResident)
        self.win.show()
    
    def updateResident(self):
        humanid = int(self.tableWidget.item(self.tableWidget.currentRow(), 3).text())
        self.data.editHuman(self.win.name.text(), self.win.surname.text(), \
               self.win.phone.text(), self.win.altphone.text(), self.win.flat.currentText(), \
               self.win.block.currentText(), humanid)
        self.populateList()
    
    def newResident(self):
        self.win = Resident(self.blocks, None, None, self)
        self.win.accepted.connect(self.createResident)
        self.win.show()
    
    def createResident(self):
        self.data.addHuman(self.win.name.text(), self.win.surname.text(), \
               self.win.phone.text(), self.win.altphone.text(), self.win.flat.currentText(), \
               self.win.block.currentText())
        self.populateList()

class Updater(QThread):
    def __init__(self, parent=None):
        super(Updater, self).__init__(parent)
        self.url = 'http://dl.dropbox.com/u/920081/dev/etalon/lastversion'
        self.exe = 'http://dl.dropbox.com/u/920081/dev/etalon/setup.exe'
        self.newver = False
    
    def go(self, progversion):
        self.progversion = progversion
        self.start()
    
    def run(self):
        f = urllib2.urlopen(self.url)
        self.webversion = float(f.read())
        if self.webversion > self.progversion:
            self.newver = True
        
class About(QDialog, ui_about.Ui_aboutDialog):
    def __init__(self, parent=None):
        super(About, self).__init__(parent)
        self.setupUi(self)
