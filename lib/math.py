#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  math.py
#  
#  Copyright 2012 Said Babayev <phoenix49@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

class Account(object):
    def __init__(self, parent=None):
        '''
        Data contains years:months:
        '''
        self.data = {}
    
    def newMonth(self, year, month):
        if not year in self.data:
            self.data[year] = []
        if month in self.data[year]:
            return 1 #Already exists
        else:
            self.data[year].append(month)
            self.data[year].sort()
            return 0
    
    def editMonth(self, oldyear, oldmonth, newyear, newmonth):
        if not oldyear == newyear:
            #Change year: save data to tmp then create new key
            tmp = self.data[oldyear]
            del self.data[oldyear]
            self.data[newyear] = tmp
        i = self.data[newyear].index(oldmonth)
        self.data[newyear][i] = newmonth
        self.data[newyear].sort()
    
    def removeMonth(self, year, month):
        i = self.data[year].index(month)
        del self.data[year][i]
    
    def getStrData(self):
        data = {}
        for year in sorted(self.data.iterkeys()):
            data[str(year)] = []
            for month in self.data[year]:
                data[str(year)].append(str(month))
        return data
    
    def getPrevMonth(self, year, month):
        ind = self.data[year].index(month)
        if not ind == 0:
            return year, self.data[year][ind - 1]
        #If month is first, try prev year
        elif year - 1 in self.data:
            return year - 1, max(self.data[year - 1])
        #if no previous year - return 0
        else:
            return 0, 0

class Statistics(object):
    def __init__(self, parent=None):
        self.residentscount = 0
        self.blockscount = 0
        self.flatscount = 0
        self.payedcurrent = 0
        self.debtcurrent = 0
    def count(self, blocks, year, month):
        self.flatscount = 0
        for block in blocks.itervalues():
            self.flatscount += len(block.flats)
        self.debtcurrent = 0
        self.payedcurrent = 0
        for block in blocks.itervalues():
            for flatobj in block.flats.itervalues():
                if flatobj.getCounter(year, month, 'payed') == 0:
                    self.debtcurrent += 1
                else:
                    self.payedcurrent += 1
    
