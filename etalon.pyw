#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       etalon.pyw
#       
#       Copyright 2012 Said Babayev <phoenix49@gmail.com>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#       

import inspect
import os.path
import sys
import pickle
from PySide.QtCore import *
from PySide.QtGui import *
from lib import resources
from lib.classes import *
from lib.math import *
from lib import ui_main

class Main(QMainWindow, ui_main.Ui_MainWindow):
    def __init__(self, parent=None):
        super(Main, self).__init__(parent)
        self.setupUi(self)
        #Initialize
        self.appname = 'Etalon'
        self.settings = QSettings("Firesoft", "Etalon")
        self.devname = 'Said Babayev'
        self.appversion = '0.2.12'
        self.filever = 2
        self.updater = Updater()
        #self.setWindowIcon(QIcon(":/download"))
        self.setWindowTitle(self.appname + ' - ' + self.appversion)
        geometry = self.settings.value("MainWindow/geometry")
        self.connected = False #Signal switch
        if geometry:
            self.restoreGeometry(geometry)
        
        #set icons and fallback icons for other platforms
        self.setWindowIcon(QIcon(":/book"))
        self.actionResidents.setIcon(QIcon(":/people"))
        self.actionBlocks.setIcon(QIcon(":/block"))
        self.actionResources.setIcon(QIcon(":/resource"))
        self.actionMonths.setIcon(QIcon(":/month"))
        
        #Initialize data
        self.readData()
        self.actionFlats.setDisabled(True) #Disable flats menu
        
        #Signals
        self.actionResidents.triggered.connect(self.editResidents)
        self.actionBlocks.triggered.connect(self.editBlocks)
        self.actionFlats.triggered.connect(self.editFlats)
        self.actionResources.triggered.connect(self.editResources)
        self.actionMonths.triggered.connect(self.editMonths)
        self.tabWidget.currentChanged.connect(self.tabChanged)
        self.actionCheck_for_updates.triggered.connect(self.updateProgram)
        self.actionAbout.triggered.connect(self.aboutShow)
        self.actionExit.triggered.connect(self.close)
        self.updater.finished.connect(self.newVersion)
        self.updateView()
    
    #DEBUG FUNCTIONS
    #Usage:
    #print self.callee(), self.caller()
    def callee(self):
        return inspect.getouterframes(inspect.currentframe())[1][1:4]

    def caller(self):
        return inspect.getouterframes(inspect.currentframe())[2][1:4]
    
    def tabChanged(self, index):
        if index == 1:
            self.calculateStats()
            self.totalResidents.setText(str(self.stats.residentscount))
            self.numberOfBlocks.setText(str(self.stats.blockscount))
            self.numberOfFlats.setText(str(self.stats.flatscount))
            self.peoplePayed.setText(str(self.stats.payedcurrent))
            self.peopleDebt.setText(str(self.stats.debtcurrent))
    
    def calculateStats(self):
        year = int(self.yearCombo.currentText()) #year name
        month = int(self.monthCombo.currentText()) #month
        self.stats.residentscount = len(self.people.pack)
        self.stats.blockscount = len(self.blocks)
        self.stats.count(self.blocks, year, month)
    
    def updateMonthView(self):
        #Disconnect signals
        self.viewWidgetConnector(False)
        #Clear all rows and update view
        for i in sorted(range(self.viewWidget.rowCount()), None, None, True):
            self.viewWidget.removeRow(i)
        #Determine current selection
        year = int(self.yearCombo.currentText()) #year name
        month = int(self.monthCombo.currentText()) #month
        block = self.blockCombo.currentText() #block name
        prevyear, prevmonth = self.account.getPrevMonth(year, month)
        #Fill tabe widget
        #Columns: Flat no, owner, ..., resources
        #Result should be a list of rows containing list of columns
        rows = []
        row = 0
        #Sum number of resources with other columns
        #Flatno, flat owner, resources, total, payed
        self.viewWidget.setColumnCount(len(self.resources) + 4)
        #Prepare table header
        header = []
        header.append(self.tr('Flat number'))
        header.append(self.tr('Flat owner'))
        for res in self.resources:
            header.append(res.name)
        header.append(self.tr('Total'))
        header.append(self.tr('Payed'))
        self.viewWidget.setHorizontalHeaderLabels(header)
        self.checkboxes = []
        for flat, flatobj in self.blocks[block].flats.iteritems():
            columns = []
            col = 0
            self.viewWidget.insertRow(row)
            #Flat number
            flatwid = QTableWidgetItem(str(flatobj.number))
            flatwid.setFlags(1|32)
            flatwid.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter)
            self.viewWidget.setItem(row, col, flatwid)
            #Flat owner
            col += 1
            if not flatobj.owner == 0:
                owner = self.people.getName(flatobj.owner)
            else:
                owner = ''
            ownerwid = QTableWidgetItem(owner)
            ownerwid.setFlags(1|32)
            self.viewWidget.setItem(row, col, ownerwid)
            #Resources
            pack = [] #list of lists with prev, cur, and price
            for res in self.resources:
                col += 1
                counters = []
                #Get prev, cur value and price
                value = flatobj.getCounter(year, month, res.name)
                prevalue = flatobj.getCounter(prevyear, prevmonth, res.name)
                counters.append(value)
                counters.append(prevalue)
                counters.append(res.price)
                pack.append(counters)
                #Res value
                self.viewWidget.setItem(row, col, QTableWidgetItem(str(value)))
            #Totals
            col += 1
            total = 0
            for counter in pack:
                price = (counter[0] - counter[1]) * counter[2]
                total += price
            #If total is negative - then it is null, incorrect value of counter or 1st month
            if total < 0:
                total = 0
            totalwid = QTableWidgetItem(str(total))
            totalwid.setFlags(1|32)
            self.viewWidget.setItem(row, col, totalwid)
            #Payed
            col += 1
            checkbox = QCheckBox()
            self.checkboxes.append(checkbox)
            checkbox.setCheckState(Qt.CheckState(flatobj.getCounter(year, month, 'payed')))
            self.viewWidget.setCellWidget(row, col, checkbox)
            checkbox.stateChanged.connect(self.boxChecked)
            row += 1
        #Connect signals back
        self.viewWidgetConnector(True)
    
    def boxChecked(self, state):
        #Determine current selection
        year = int(self.yearCombo.currentText()) #year name
        month = int(self.monthCombo.currentText()) #month
        block = self.blockCombo.currentText() #block name
        for checkbox in self.checkboxes:
            flat = int(self.viewWidget.item(self.checkboxes.index(checkbox), 0).text())
            self.blocks[block].flats[flat].updateCounter(year, month, 'payed', int(checkbox.checkState()))
    
    def itemChanged(self, row, col):
        #Determine current selection
        year = int(self.yearCombo.currentText()) #year name
        month = int(self.monthCombo.currentText()) #month index
        block = self.blockCombo.currentText() #block name
        #Find out what is changed and its value
        value = int(self.viewWidget.item(row, col).text())
        res = self.viewWidget.horizontalHeaderItem(col).text()
        flat = int(self.viewWidget.item(row, 0).text())
        self.blocks[block].flats[flat].updateCounter(year, month, res, value)
        self.updateMonthView()
    
    def viewWidgetConnector(self, state):
        if state is True:
            if self.connected is False:
                self.yearCombo.currentIndexChanged.connect(self.updateMonthCombo)
                self.monthCombo.currentIndexChanged.connect(self.updateMonthView)
                self.blockCombo.currentIndexChanged.connect(self.updateMonthView)
                self.viewWidget.cellChanged.connect(self.itemChanged)
            self.connected = True
        else:
            if self.connected is True:
                self.yearCombo.currentIndexChanged.disconnect()
                self.monthCombo.currentIndexChanged.disconnect()
                self.blockCombo.currentIndexChanged.disconnect()
                self.viewWidget.cellChanged.disconnect()
            self.connected = False
    
    def updateView(self):
        #Update month view combos
        data = self.account.getStrData()
        #Disconnect combos
        self.viewWidgetConnector(False)
        #Update combos
        self.yearCombo.clear()
        self.yearCombo.addItems(data.keys())
        self.updateMonthComboLocal()
        self.blockCombo.clear()
        self.blockCombo.addItems(self.blocks.keys())
        try:
            self.updateMonthView()
        except:
            pass
        #Connect combos
        self.viewWidgetConnector(True)
        
    def updateMonthComboLocal(self):
        data = self.account.getStrData()
        self.monthCombo.clear()
        if not self.yearCombo.currentIndex() == -1:
            self.monthCombo.addItems(data[self.yearCombo.currentText()])
    
    def updateMonthCombo(self):
        data = self.account.getStrData()
        try:
            self.monthCombo.currentIndexChanged.disconnect()
        except:
            pass
        self.monthCombo.clear()
        if not self.yearCombo.currentIndex() == -1:
            self.monthCombo.addItems(data[self.yearCombo.currentText()])
        try:
            self.monthCombo.currentIndexChanged.connect(self.updateMonthView)
        except:
            pass
        self.updateMonthView()
        
    def editMonths(self):
        self.monwin = Months(self.account, self)
        self.monwin.accepted.connect(self.saveData)
        self.monwin.show()
    
    def editResources(self):
        self.resourcewin = Resources(self.resources, self)
        self.resourcewin.accepted.connect(self.saveData)
        self.resourcewin.show()
    
    def editFlats(self):
        #future
        self.flwin = Flats(self.blocks, self.people, self)
        self.flwin.show()
    
    def editResidents(self):
        self.reswin = Residents(self.people, self.blocks, self)
        self.reswin.accepted.connect(self.saveData)
        self.reswin.show()
    
    def editBlocks(self):
        self.blwin = Blocks(self.blocks, self)
        self.blwin.accepted.connect(self.saveData)
        self.blwin.show()
    
    def saveData(self):
        #Save data to file
        if hasattr(self, 'reswin'):
            self.people = self.reswin.data
        if hasattr(self, 'blwin'):
            self.blocks = self.blwin.data
        if hasattr(self, 'monwin'):
            self.account = self.monwin.account
        if hasattr(self, 'resourcewin'):
            self.resources = self.resourcewin.resources
        f = open('data.etl', 'wb')
        pickle.dump(self.filever, f)
        pickle.dump(self.people, f)
        pickle.dump(self.blocks, f)
        pickle.dump(self.resources, f)
        pickle.dump(self.account, f)
        pickle.dump(self.stats, f)
        self.syncData()
        f.close()
        self.updateView()
    
    def readData(self):
        #Read file data
        if os.path.exists('data.etl'):
            f = open('data.etl', 'rb')
        else:
            #Init new data
            self.people = People()
            self.blocks = {} #Blocklabel : Block
            self.resources = [] #List of Resource()
            self.account = Account()
            self.stats = Statistics()
        try:
            filever = pickle.load(f)
        except:
            return
        if filever == self.filever:
            self.people = pickle.load(f)
            self.blocks = pickle.load(f)
            self.resources = pickle.load(f)
            self.account = pickle.load(f)
            self.stats = pickle.load(f)
        elif filever == 1:
            self.people = pickle.load(f)
            self.blocks = pickle.load(f)
            self.resources = pickle.load(f)
            self.account = pickle.load(f)
            self.stats = Statistics()
        else:
            #Old version, initialize new data
            self.people = People()
            self.blocks = {} #Blocklabel : Block
            self.resources = [] #List of Resource()
            self.account = Account()
            self.stats = Statistics()
    
    def syncData(self):
        #Sync data between containers
        #Clear existing flat owners
        for block in self.blocks:
            for flat in self.blocks[block].flats:
                self.blocks[block].flats[flat].owner = 0
        #Update with new info
        for huid in self.people.pack.iterkeys():
            try:
                fl = int(self.people.pack[huid].livesinflat)
                bl = self.people.pack[huid].livesinblock
                self.blocks[bl].flats[fl].owner = huid
            except:
                pass
    
    def closeEvent(self, event):
        #Reimplement close event, event is a var and save window settings
        self.settings.setValue("MainWindow/geometry", self.saveGeometry())
        self.saveData()
        event.accept()
    
    def updateProgram(self):
        self.updater.go(float(self.appversion[:self.appversion.rfind('.')]))
    
    def newVersion(self):
        if self.updater.newver:
            box = QMessageBox(self)
            box.setWindowTitle(self.tr('New version'))
            box.setTextFormat(Qt.RichText)
            box.setText("New version available. Download from: \n <a href='{0}'>Etalon version {1}</a>".format(self.updater.exe, self.updater.webversion))
            box.show()
        
    def aboutShow(self):
        self.dialog = About(self)
        self.dialog.logoLabel.setPixmap(QPixmap(":/book").scaled(64, 64, Qt.KeepAspectRatio, Qt.FastTransformation))
        self.dialog.setWindowTitle(self.tr('About ')+ self.appname)
        self.dialog.titleLabel.setText(self.appname)
        self.dialog.versionLabel.setText(self.tr('version ') + self.appversion)
        self.dialog.aboutLabel.setText(self.tr('Application for accounting in building management'))
        self.dialog.rightsLabel.setText(self.tr('Copyright') + ' (c) 2012 ' + self.devname)
        self.dialog.connect(self.dialog.closeButton, SIGNAL("clicked()"), self.dialog.close)
        self.dialog.show()

def main():
    app = QApplication(sys.argv)
    #Findout syslocale
    locale = QLocale.system().name()
    #find and activate some system translations
    #qtTranslator = QTranslator()
    #if qtTranslator.load("qt_" + locale):
    #    app.installTranslator(qtTranslator)
    #custom translations
    #appTranslator = QTranslator()
    #if appTranslator.load(":/dit_" + locale):
    #    app.installTranslator(appTranslator)
    window = Main()
    window.show()
    app.exec_()
    return 0

if __name__ == '__main__':
    main()

