<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="az_AZ" sourcelanguage="en">
<context>
    <name>BlockDialog</name>
    <message>
        <location filename="../lib/classes.py" line="433"/>
        <source>Block name: </source>
        <translation>Blokun adı:</translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="435"/>
        <source>Number of flats: </source>
        <translation>Mənzillərin sayı:</translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="438"/>
        <source>Number of floors: </source>
        <translation>Mərtəbələrin sayı:</translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="442"/>
        <source>Cancel</source>
        <translation>İmtina</translation>
    </message>
</context>
<context>
    <name>Blocks</name>
    <message>
        <location filename="../lib/classes.py" line="460"/>
        <source>Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="461"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="461"/>
        <source>Number of flats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="462"/>
        <source>Number of floors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="484"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="478"/>
        <source>Are you sure you wish to delete {0}?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="485"/>
        <source>Are you sure you wish to delete all blocks?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="494"/>
        <source>Edit Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="503"/>
        <source>New Block</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FlatDialog</name>
    <message>
        <location filename="../lib/classes.py" line="338"/>
        <source>Flat number: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="341"/>
        <source>Flat owner: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="343"/>
        <source>Number of residents: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="346"/>
        <source>Area: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="349"/>
        <source>Rooms: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="353"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Flats</name>
    <message>
        <location filename="../lib/classes.py" line="375"/>
        <source>Flats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="376"/>
        <source>Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="376"/>
        <source>Flat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="377"/>
        <source>Owner</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../etalon.pyw" line="123"/>
        <source>Flat number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../etalon.pyw" line="124"/>
        <source>Flat owner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../etalon.pyw" line="127"/>
        <source>Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../etalon.pyw" line="128"/>
        <source>Payed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../etalon.pyw" line="370"/>
        <source>New version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../etalon.pyw" line="378"/>
        <source>About </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../etalon.pyw" line="380"/>
        <source>version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../etalon.pyw" line="381"/>
        <source>Application for accounting in building management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../etalon.pyw" line="382"/>
        <source>Copyright</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MonthDialog</name>
    <message>
        <location filename="../lib/classes.py" line="313"/>
        <source>Year: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="314"/>
        <source>Month: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="320"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Months</name>
    <message>
        <location filename="../lib/classes.py" line="236"/>
        <source>Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="237"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="238"/>
        <source>Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="257"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="251"/>
        <source>Are you sure you wish to delete {0}?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="258"/>
        <source>Are you sure you wish to delete all months?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="268"/>
        <source>Edit month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="286"/>
        <source>New month</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ResDialog</name>
    <message>
        <location filename="../lib/classes.py" line="143"/>
        <source>Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="144"/>
        <source>Price: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="150"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Residents</name>
    <message>
        <location filename="../lib/classes.py" line="530"/>
        <source>Residents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="536"/>
        <source>Flat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="536"/>
        <source>Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="536"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="559"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="553"/>
        <source>Are you sure you wish to delete {0}?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="560"/>
        <source>Are you sure you wish to delete all people?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Resources</name>
    <message>
        <location filename="../lib/classes.py" line="167"/>
        <source>Resources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="168"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="169"/>
        <source>Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="178"/>
        <source>New resource</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="192"/>
        <source>Edit resource</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="215"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="209"/>
        <source>Are you sure you wish to delete {0}?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/classes.py" line="216"/>
        <source>Are you sure you wish to delete all resources?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
